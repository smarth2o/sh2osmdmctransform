package it.pig;

import org.apache.pig.pigunit.PigTest;
import org.apache.pig.tools.parameters.ParseException;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static junit.framework.Assert.assertEquals;

/**
 * 
 * @author Cosmin Iordache
 * This tests that the UDFs written work accordingly . At the moment only one
 * UDF is written for SES.
 *
 * First test (udfTest) is a simple test to see if the sesudf UDF is parsing the
 * information correctly. The 2nd (pigLoadTest) and the 3rd ( pigProcTest) test
 * are the actual scripts that get run on hadoop that generate the final file.
 * These scripts run in the integration test phase .
 */
public class BagIT {

	private static final String TEST_PATH = "src/test/resources/";
	private static PigTest udfTest, pigLoadTest, pigProcTest;

	@BeforeClass
	public static void setup() throws IOException, ParseException {

		udfTest = new PigTest("./src/test/resources/sesudftest.pig", new String[] {}); // no
																						// parameters
		udfTest.override("readings", "readings = LOAD '" + TEST_PATH + "/input/inputmultiple.txt' USING PigStorage() as (onexml:chararray);");

		pigLoadTest = new PigTest("./src/test/resources/SESload.pig", new String[] { "input_folder=src/test/resources/input/inputmultiple.txt",
				"processed_file_folder=src/test/resources/input/processed_file" });

		pigProcTest = new PigTest("./src/test/resources/SESproc.pig", new String[] { "input_folder_houserel=src/test/resources/input/houserel",
				"processed_file_folder=src/test/resources/input/processed_file", "output_folder=src/test/resources/output" });
	}

	@Test
	public void testOutputofUDF() {

		try {
			udfTest.assertOutput("B", new File(TEST_PATH + "check/output.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testOutputofPigLoad() {

		try {
			pigLoadTest.assertOutput("F", new File(TEST_PATH + "check/processed_file"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testOutputofPigProc() {

		try {
			pigProcTest.assertOutput("final", new File(TEST_PATH + "check/final"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
