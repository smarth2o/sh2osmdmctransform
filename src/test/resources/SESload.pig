REGISTER ./target/smarth2oudfs.jar;
/* the register line  is different in test vs main workflow/SESload vs test/SESload be carefull to change */
DEFINE sesbag udfs.SESBag();
/* load file with multiple lines of XML. */

A = load '$input_folder' using PigStorage() as (onexml:chararray);
/* parse and load in a bag that contains tuple with each reading */
B = FOREACH A GENERATE sesbag(onexml) ;
/* generate the tuple */
C = FOREACH B GENERATE FLATTEN(bag_of_metertuples) as (meterid:chararray, consumption:double, timestamp:chararray);

/* remove duplicates */
E = GROUP C BY ($0,$2);
F = FOREACH E {top_rec = LIMIT C 1;GENERATE FLATTEN(top_rec);};
illustrate F;
/* store */
store F into '$processed_file_folder'  using PigStorage(',');
