REGISTER ./target/smarth2oudfs.jar;
DEFINE sesbag udfs.SESBag();

-- Load xml entries from hdfs
readings =  LOAD '/some/path/users.txt' USING PigStorage() as (onexml:chararray);
/* parse and load in a bag that contains tuple with each reading */ 
B = FOREACH readings GENERATE sesbag(onexml) ;

