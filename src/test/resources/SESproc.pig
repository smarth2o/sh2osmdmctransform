/* load the house relationship in order to populate houseid field */
houserel = LOAD '$input_folder_houserel' using PigStorage(',') as (houseid:chararray, meterid:chararray);
/* join by meterid */
processed_file= LOAD '$processed_file_folder' using PigStorage(',') as (meterid:chararray , consumption:chararray, timestamp:chararray);
illustrate processed_file;
illustrate houserel;
joined = JOIN processed_file by meterid, houserel by meterid USING 'replicated';
illustrate joined;
/* generate file to copy to the database */
final = FOREACH joined GENERATE  processed_file::meterid, processed_file::timestamp, houserel::houseid, processed_file::consumption ;
illustrate final;
/* finally store */
store final into '$output_folder'  using PigStorage(',');




