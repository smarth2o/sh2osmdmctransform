REGISTER smarth2oudfs.jar;
REGISTER jdom-2.0.5.jar; 
/* the register line  is different in test vs main workflow/SESload vs test/SESload and test/sesudfs be carefull to change */
/*  This is the loader part of the script - it takes a file that contains multiple lines with a fully formed XML on each line */  

DEFINE sesbag udfs.SESBag();
/* load file with multiple lines of XML. */

A = load '$input_folder' using PigStorage() as (onexml:chararray);
/* parse and load in a bag that contains tuple with each reading */
B = FOREACH A GENERATE sesbag(onexml) ;
/* generate the tuple */
C = FOREACH B GENERATE FLATTEN(bag_of_metertuples) as (meterid:chararray, consumption:double, timestamp:chararray);

/* remove duplicates */
E = GROUP C BY ($0,$2);
F = FOREACH E {top_rec = LIMIT C 1;GENERATE FLATTEN(top_rec);};
G = FOREACH F GENERATE null,timestamp, meterid, 'SES', consumption, null;
/* illustrate F; */
/* store */
store G into '$processed_file_folder'  using PigStorage(',');
