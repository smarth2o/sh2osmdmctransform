A = LOAD '$input_folder_household' using PigStorage(',') as (meter_id:chararray,timestamp:chararray,total_consumption:double, hoid:chararray);
B = FOREACH A GENERATE meter_id, CONCAT(STRSPLIT(timestamp, ' ', 2).$0, ' 00:00:00'), total_consumption, hoid;
C = group B by (meter_id, $1);
D = FOREACH C GENERATE FLATTEN(group), MAX(B.total_consumption)- MIN(B.total_consumption), FLATTEN(BagToTuple(B.hoid));
E = FOREACH D GENERATE $3 as hoid, $1 as timestamp, $2 as daily_consumption;
F = group E by (hoid, timestamp);
G = FOREACH F GENERATE FLATTEN(group) as (hoid, timestamp), SUM(E.daily_consumption) as daily_consumption;
H = FOREACH G GENERATE null, daily_consumption, timestamp, ToString(AddDuration(ToDate(timestamp, 'yyyy-MM-dd HH:mm:ss'), 'P1D'), 'yyyy-MM-dd HH:mm:ss'), hoid;
store H into '$output_folder'  using PigStorage(',');