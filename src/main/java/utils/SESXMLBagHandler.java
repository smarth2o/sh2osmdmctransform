package utils;

import java.util.ArrayList;
import java.util.List;
import java.io.StringReader;
import java.io.IOException;
import java.lang.String;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.Attributes;

import utils.Utils;
/**
 * @author Stan Dragos 
 * @author Cosmin Iordache
 *This is the handler for the tuple parser. This gets a XML tuple containing one or more meterreading entries and 
 *unmarshalls the tuple and returns the  extracted entries .
 *
 */


public class SESXMLBagHandler {

	private List<MeterReading> entrylist = null;
	private MeterReading entry = null;
	private String meterId = "";
	public List<MeterReading> getEntryList() {
		return entrylist;
	}

	public void read(String xml) throws IOException {
		entrylist = new ArrayList<MeterReading>();
		SAXBuilder saxBuilder = new SAXBuilder();
		try {
			Document document = saxBuilder.build(new StringReader(xml));
			Element rootNode = document.getRootElement();
		//	List<Element> allEntries = rootNode.getChildren("register");
			List<Element> allEntries = rootNode.getChildren("dynamic-data").get(0).getChildren("registers").get(0).getChildren("register");
			for(int i=0;i<=allEntries.size()-1;i++){
				Element element = allEntries.get(i);
				String val = element.getAttribute("value-id").getValue();
				if (!element.getAttribute("metering-point-name").equals(null) && !val.equalsIgnoreCase("1-0:0.0.0")){
					String mid = element.getAttribute("metering-point-name").getValue();
					entry= new MeterReading();
					entry.setMeterId(mid);
					String mread = element.getChildText("value");
					entry.setConsumption(Double.valueOf(mread));
			                String mtime = element.getAttribute("timestamp").getValue();
					entry.setReadingDatetime(Utils.getTimestampFromSESTimestamp(mtime));
					entrylist.add(entry);
					} 
		       };
		} catch (JDOMException e) {
		    throw new  IOException("SESBag ", e);
		} catch (IOException e) {
		    e.printStackTrace();
	}}
}
