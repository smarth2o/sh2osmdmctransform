package utils;
import java.sql.Timestamp;

/**
 * Model class for Meterreading
 */
public class MeterReading implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private String meterId;
	private Timestamp readingDatetime;
	private Integer houseId;
	private Double consumption;

	public String getMeterId() {
		return meterId;
	}

	public void setMeterId(String meterId) {
		this.meterId = meterId;
	}

	public Timestamp getReadingDatetime() {
		return readingDatetime;
	}

	public void setReadingDatetime(Timestamp readingDatetime) {
		this.readingDatetime = readingDatetime;
	}

	public Integer getHouseId() {
		return houseId;
	}

	public void setHouseId(Integer houseId) {
		this.houseId = houseId;
	}

	public Double getConsumption() {
		return consumption;
	}

	public void setConsumption(Double consumption) {
		this.consumption = consumption;
	}

	@Override
	public String toString() {
		return "MeterReading [meterId=" + meterId + ", readingDatetime="
				+ readingDatetime + ", houseId=" + houseId + ", consumption="
				+ consumption + "]";
	}
	
	

}
