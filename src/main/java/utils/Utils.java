package utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

public class Utils {
	private static final Logger LOG = Logger.getLogger(Utils.class);
	
	private Utils() {
		
	}
	
	public static Timestamp getTimestampFromSESTimestamp(String sesTimestamp) {
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		 sdf.setTimeZone(TimeZone.getTimeZone("CET"));
		try {
	// change timezone from local ses time to utc
			Date date = sdf.parse(sesTimestamp);
			sdf = new SimpleDateFormat();
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date gmtTime = new Date(sdf.format(date));
			return new Timestamp(gmtTime.getTime());
		} catch (ParseException e) {
			LOG.error("Error when parsing SESTimestamp",e);
		}
		
		return null;
	}
	

	public static Timestamp getTimestampFromDBTimestampString (String dbTimestamp ) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
	// change timezone from utc to local ses time
			Date date  = sdf.parse(dbTimestamp);
			sdf = new SimpleDateFormat();
			sdf.setTimeZone(TimeZone.getTimeZone("CET"));
			Date localTime = new Date(sdf.format(date));
			return new Timestamp(localTime.getTime());
		
		} catch (ParseException e) {
			LOG.error("Error when parsing dbTimestamp" + dbTimestamp, e);
		}
		return null;
		
	}

}
