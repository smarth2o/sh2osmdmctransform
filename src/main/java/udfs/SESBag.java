package udfs;

import java.io.IOException;
import java.io.StringReader;

import org.apache.log4j.Logger;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.util.WrappedIOException;

import utils.MeterReading;
import utils.SESXMLBagHandler;

/**
 * 
 * @author  Cosmin Iordache 
 * 		This is specific to SES XML file! 
 * 		This function is used to parse a tuple ( so one line of a
 *         flat XML file ) that contains reading entries and output it as a bag
 *         with each tuple containing a meterreading. 
 *         This uses a SAXParser that needs to be modified whenever the XML gets modified
 *
 */
public class SESBag extends EvalFunc<DataBag>  {
	public static final Logger LOG= Logger.getLogger(SESBag.class);
	public static final String TAG = "SESBag";
			TupleFactory mTupleFactory = TupleFactory.getInstance();
	BagFactory mBagFactory = BagFactory.getInstance();
	

	@Override
	public DataBag exec(Tuple input) throws IOException {

		int argsSize = input.size();

		if (input == null || argsSize == 0) {
			return null;
		}

		DataBag bag = mBagFactory.newDefaultBag();

		try {
			SESXMLBagHandler handler = new SESXMLBagHandler();
			String xml_string = "";
			for (int i = 0; i < input.size(); i++){
				xml_string = xml_string + (String) input.get(i);
			}
			//handler.read((String)input.get(0));
			System.out.println("input xml_string:" + xml_string );
			handler.read(xml_string);
//			LOG.info("input tuple:" + (String) input.get(0) );
			LOG.info("EntryList:" + handler.getEntryList().size() + handler.getEntryList().toArray().toString());
//			System.out.println("input tuple:\n\n" + (String) input.get(0));
//			System.out.println("EntryList:\n\n" + handler.getEntryList().size() + handler.getEntryList().toArray().toString());
			for (MeterReading entry : handler.getEntryList()) {
				LOG.info("Entry:" + entry.toString() );
//				System.out.println("Entry:" + entry.toString());
				Tuple tuple = TupleFactory.getInstance().newTuple(3);
				tuple.set(0, entry.getMeterId().toString());
				tuple.set(1, entry.getConsumption());
				tuple.set(2, entry.getReadingDatetime().toString());
				bag.add(tuple);
			}
// debug
//			for (Tuple i : bag){
//			System.out.println("("+i.toDelimitedString(",")+"),");
//			}


			return bag;
		} catch (IOException e) {
			throw new  IOException("SESBag - ParseConfiguration ", e);
		}
	}

	@Override
	public Schema outputSchema(Schema input)  {
		if (input == null) {
			return null;
		}
		try {
			Schema tupleSchema = new Schema();
			Schema.FieldSchema meterid = new Schema.FieldSchema("meterid", DataType.CHARARRAY);
			Schema.FieldSchema consumption = new Schema.FieldSchema("consumption", DataType.DOUBLE);
			Schema.FieldSchema timestamp = new Schema.FieldSchema("timestamp", DataType.CHARARRAY);
			tupleSchema.add(meterid);
			tupleSchema.add(consumption);
			tupleSchema.add(timestamp);
			Schema.FieldSchema tupleFieldSchema = new Schema.FieldSchema("tuple_of_meterreadings", tupleSchema, DataType.TUPLE);

			Schema bagSchema = new Schema(tupleFieldSchema);
			bagSchema.setTwoLevelAccessRequired(true);
			Schema.FieldSchema bagFieldSchema = new Schema.FieldSchema("bag_of_metertuples", bagSchema, DataType.BAG);

			return new Schema(bagFieldSchema);
		} catch (Exception e) {
			return null;
		}

	}

}
